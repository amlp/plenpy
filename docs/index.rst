.. plenpy documentation master file, created by
   sphinx-quickstart on Fri May 12 17:28:30 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to plenpy's documentation!
==================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   Introduction <intro.md>
   How To Use <how-to.rst>
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

